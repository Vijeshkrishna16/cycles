var express=require('express');
var router=express.Router();
var cycle=require('../schema/cycle.js');

router.post('/add' , function (req, res) {
   cycle.findOneAndUpdate({cycleId:req.body.cycle_id},{cycleName:req.body.cycle_name,cycleId:req.body.cycle_id},{upsert: true},function(err,cycledoc) {
   if(cycledoc == null)
    {
      cycle.findOne({cycleId:req.body.cycle_id}).sort({cycleId:-1}).exec(function(err,insertedcycledoc){
         res.json({'status':0,'Message':'Cycle Inserted'}); 
       });
    }
    else
    {
      cycle.findOne({cycleId:cycledoc.cycleId},function(err,updatedcycledoc){
         res.json({'status':0,'Message':'Cycle updated'}); 
       });
    } 
  });
});

//get cycle details
router.post('/get',function(req,res){
  cycle.findOne({'cycleId':req.body.cycle_id}, function (err,cycle){
    if (cycle==null)
    {
      res.json({"status":1,"err":"cycle not found."});
      return;
    } 
    else
    {
      res.json(cycle);
      
    }
  });
});  
module.exports=router;