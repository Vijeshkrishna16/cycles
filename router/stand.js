var express=require('express');
var router=express.Router();
var stand=require('../schema/stand.js');
var cycle=require('../schema/cycle.js');
var standlog=require('../schema/log.js');

// insert new stand
router.post('/add' , function (req, res) {
  stand.findOneAndUpdate({imei:req.body.imei},{standName:req.body.stand_name,imei:req.body.imei},{upsert: true},function(err,standdoc) {
    if(standdoc == null)
    {
      stand.findOne({imei:req.body.imei}).sort({imei:-1}).exec(function(err,insertedstanddoc){
         res.json({'status':0,'Message':'Stand Inserted'}); 
       });
    }
    else
    {
      stand.findOne({imei:standdoc.imei},function(err,updatedstanddoc){
         res.json({'status':0,'Message':'Stand updated'}); 
       });
    } 
  });

});

// insert new log
router.post('/addlog' , function (req, res) {
  standlog.findOne({'txnId':req.body.txnid},function(err,txn){
     var txnid=req.body.txnid;
    if(txn == null)
    {
       stand.findOne({ 'imei': req.body.imei }, '_id', function (err, stand) {
        if (stand==null)
        {
          res.json({"status":1,"err":"stand not found."});
          return;
        } 
        else 
        {
          var standId=stand._id;
          var cycleList=req.body.cycles;
          var inputCycleArr=cycleList.split(",");
          var logTime = req.body.logTime;
          cyclesArr=[];
          cycle.find({"cycleId":{$in:inputCycleArr}},function(err,docs) {
          if(docs!=null) 
          {
            docs.forEach(function(doc) {
            cyclesArr.push(doc._id);
            });
            logDoc=new standlog({
            standId:standId,
            cycles:cyclesArr,
            logTime:new Date(Date.parse(logTime)),
            txnId:txnid
            });
            logDoc.save();
            res.json({"status":0,txnid:txnid});
            return;
          }
          else 
          {
            res.json({"status":1,"err":"cycles not found."});
            return;
          }
          });
        }
      });
    }
    else
    {
      res.json({"status":0,txnid:txnid});
       return;
    }
  });
});

//get log of a particular stand
router.post('/getlog',function(req,res){
  stand.findOne({'imei':req.body.imei},'_id standName', function (err,stand){
    if (stand==null)
    {
      res.json({"status":1,"err":"stand not found."});
      return;
    } 
    else
    {
       standlog.findOne({'standId':stand._id}).sort({logTime: -1}).populate('cycles').exec(function (err ,logdoc){
        res.json({"Stand":stand.standName,"No of Cycles":logdoc.cycles.length,"Cycles":logdoc.cycles});
     });
    }
  });
});
// get log of all stands
router.post('/getalllog',function(req,res){
  stand.find({},'_id standName', function (err,stand){
    standlog.find({'standId':stand._id}).sort({logTime: -1}).populate('cycles standId').exec(function (err ,logdoc){
      var listcycles=[];
      logdoc.forEach(function(doc){
      listcycles.push({"Stand":doc.standId.standName,"No of cycles":doc.cycles.length,"Cycles":doc.cycles});
      });
      res.json(listcycles);      
     });    
  });
});
module.exports=router;
