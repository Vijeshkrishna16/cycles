var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var log = new Schema({
	 standId: { type: Schema.ObjectId, ref: 'stand' },
	 cycles:[ {type: Schema.ObjectId, ref: 'cycle'} ],
	logTime: { type : Date },
	txnId: String

});
// the schema is useless so far
// we need to create a model using it
var logModel = mongoose.model('log',log);

// make this available to our users in our Node applications
module.exports = logModel;


