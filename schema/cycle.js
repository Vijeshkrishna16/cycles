var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//create a schema
var cycle= new Schema({
	cycleName:String,
	cycleId:String
});

cycle.set('toJSON', {
    transform: function(doc, ret, options) {
        var retJson = {
            cycleName: ret.cycleName,
            cycleId: ret.cycleId
            
        };
        return retJson;
    }
});
// the schema is useless so far
// we need to create a model using it
var cycleModel = mongoose.model('cycle', cycle);

// make this available to our users in our Node applications
module.exports = cycleModel;