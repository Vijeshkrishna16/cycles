var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//create a schema
var stand = new Schema({
	standName:String,
	imei:String
});
stand.set('toJSON', {
    transform: function(doc, ret, options) {
        var retJson = {
            standName: ret.standName,
            imei: ret.imei
            
        };
        return retJson;
    }
});
// the schema is useless so far
// we need to create a model using it
var standModel = mongoose.model('stand', stand);
// make this available to our users in our Node applications
module.exports = standModel;